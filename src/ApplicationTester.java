import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ApplicationTester {
    @Mock
    CalculatorService calcService;

    @InjectMocks
    Application app=new Application();

    @Test
    public void testingAdd(){
        when(calcService.add(10,20)).thenReturn(30);
        int actual = app.addition(10,20);
        int expected = 30;
        Assert.assertEquals(expected,actual);
    }
    @Test
    public void testSubtraction(){
        when(calcService.diff(20,15)).thenReturn(5);
        int response = app.subtraction(20,15);
        Assert.assertEquals(5,response);
    }
}
