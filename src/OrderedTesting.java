import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OrderedTesting {
    private Application app;
    private CalculatorService calculatorService;

    @Before
    public void init(){
        app= new Application();
        calculatorService =mock(CalculatorService.class);
        app.setCalculatorService(calculatorService);
    }
    @Test
    public void orderTest(){
        when(calculatorService.add(20,34)).thenReturn(54);
        when(calculatorService.diff(34,24)).thenReturn(10);

        Assert.assertEquals(10,app.subtraction(34,24));
        Assert.assertEquals(54,app.addition(20,34));

        InOrder inOrder = Mockito.inOrder(calculatorService);
        inOrder.verify(calculatorService).diff(34,24);
        inOrder.verify(calculatorService).add(20,34);


    }

}
