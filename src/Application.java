public class Application {
    private CalculatorService cs;
    public void setCalculatorService(CalculatorService c)
    {
        this.cs=c;
    }
    public int addition(int a,int b){
        return cs.add(a,b);
    }
    public int subtraction(int a,int b){
        return cs.diff(a,b);

    }
    public int product(int a, int b){
        return cs.product(a, b);
    }
    public int division(int a,int b){
        return cs.division(a,b);
    }
    public int remainder(int a,int b){
        return cs.remainder(a,b);
    }
}
