public interface CalculatorService {
    public int add(int a,int b);
    public int diff(int a,int b);
    public int product(int a,int b);
    public int division(int a,int b);
    public int remainder(int a,int b);
}
